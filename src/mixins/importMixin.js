function importComponents(components) {


    if(components) {
        for (let component in components) {
            // console.log(component);
           import('../components/' + component);
        }
    }
}

export default {
    run(components) {
        importComponents(components);
    }
};