import Vue from "vue";

/**
 * Affectation globale d'une configuration
 */
import { config } from "./config/config";

Vue.prototype.appConfig = config;



/**
 * Wrapper VueJS
 */
import "./utils/VueWrapper";

/**
 * Import des composants
 */
import "./components/simpleComponent/";
import "./components/simpleComponentProp/";



// ne fonctionne pas
// import importMixin from "./mixins/importMixin";
// importMixin.run(config.components);



