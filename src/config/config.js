let config;

config = {
  title: "Titre de la page HTML",
  version: "1.0.0",
  components : {
    simpleComponent : 'simpleComponent',
    simpleComponentProp : 'simpleComponentProp'
  }
};


/**
 * TODO : creation d'un composant pour gérer les header HTML
 */

document.title= config.title;

export { config };
