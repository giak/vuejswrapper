import Vue from "vue";

/**
 * Wrapper VueJs pour instancier N components
 * Ex : window.vueWrapper.init();
 * Ex : <call-app vue-attr></call-app>
 * @type {{init: vueWrapper.init}}
 */
export let vueWrapper = {
  init: function(selector = "[vue-attr]") {
    let elements = [].slice.call(document.querySelectorAll(selector));
    elements.forEach(function(element) {
      return new Vue({
        el: element
      });
    });
  }
};
window.vueWrapper = vueWrapper;
