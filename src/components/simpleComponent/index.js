import Vue from "vue";

// application root
import simpleComponentApp from "./simpleComponentApp";

Vue.component("simple-component", simpleComponentApp);
