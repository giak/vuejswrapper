import Vue from "vue";

// application root
import simpleComponentPropApp from "./simpleComponentPropApp";

Vue.component("simple-component-prop", simpleComponentPropApp);
